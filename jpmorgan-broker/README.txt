MAIN MENU
=========

1. Running the program will present the menu below:
	+---------------------------------------------------------------------------------+
	|                                      MENU                                       |
	+---------------------------------------------------------------------------------+
	|    Press:                                                                       |
	|         1 ==> To Send an instruction                                            |
	|         2 ==> To Instruction report                                             |
	+---------------------------------------------------------------------------------+
	
	Option 1 serves like an input funcionality to send instruction to the system.
	Option 2 generates the report.

----------------------------------------------------------------------------------------

SENDING INSTRUCTIONS
====================

1. In the Main Menu pick option number 1 ==> To Send an instruction.

2. Type then Entity name, eg.: VFR, BGT, PACTUAL

3. Choose the Instruction Type - B = BUY / S SELL

4. Type the Agreed Fx - Decimal

5. Type the currency code, eg.: SGP, AED, BRL

6. Type the unit quantity

7. Type the price per unit - Decimal

8. System will present the message "Instruction sent successfully." and the instruction data recorded like below.

	Entity: PETROBRAS
	Instruction Type: BUY
	Agreed Fx: 1.55
	Currency: SGP
	Instruction Date: 07/23/2017
	Settlement Date: 07/23/2017
	Units: 78
	Price Per Unit: 5.66
	USD Amount: 684.2940000000001

9. In the next menu choose 1 To principal Menu or 2 To Finish

----------------------------------------------------------------------------------------

GENERATING REPORT

1. In the Main Menu pick option 2 ==> To instruction report

2. Type the instruction Date to generate the report

3. The system will present two frames:

	+---------------------------------------------------------------------------------+
	|                             AMOUNT IN USD on 07/23/2017                         |
	+---------------------------------------------------------------------------------+
	|    Incoming amount: USD           5,211.54                                      |
	|    Outgoing amount: USD             607.29                                      |
	+---------------------------------------------------------------------------------+
	
	And
	
	+---------------------------------------------------------------------------------+
	|                             RANKED ENTITIES on 07/23/2017                       |
	+---------------------------------------------------------------------------------+
	|                                                                                 |
	|    Incoming:                                                                    |
	|                     PACTUAL: USD           4,611.77                             |
	|                   PETROBRAS: USD             357.08                             |
	|                    BRADESCO: USD             242.70                             |
	|                                                                                 |
	|    Outgoing:                                                                    |
	|                BRASIL FOODS: USD             581.56                             |
	|                         MNH: USD              25.73                             |
	|                                                                                 |
	+---------------------------------------------------------------------------------+
	
4. In the next menu choose 1 To principal Menu or 2 To Finish


