package com.jpmorgan.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.jpmorgan.console.tasks.Task;
import com.jpmorgan.enums.MainMenuEnum;

public class App
{
	public static BufferedReader BR = new BufferedReader(new InputStreamReader(System.in));
	public static final String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";
	
    public static void main( String[] args )
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	sb.append("|                                                                                 |").append("\n");
    	sb.append("|                           J.P MORGAN CONSOLE BROKER                             |").append("\n");
    	sb.append("|                                                                                 |").append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	sb.append("|                                    WELCOME                                      |").append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	
    	print(sb.toString());
    	showMainMenu();
    }
    
    public static void showMainMenu(){
    	StringBuilder sb = new StringBuilder();
    	sb.append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	sb.append("|                                      MENU                                       |").append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");    	
    	sb.append("|    Press:                                                                       |").append("\n");
    	for( MainMenuEnum menuOptions : MainMenuEnum.values() ){
    		sb.append("|        ").append(String.format("%2s", menuOptions.getId())).append(" ==> ").append(String.format("%-22s", menuOptions.getDescription())).append("                                            |").append("\n");
    	}
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	
    	Integer selectedOption = null;
    	MainMenuEnum mainMenuEnum = null;
    	do{
    		selectedOption = getInt(sb.toString());
    		if( selectedOption != null ){
    			mainMenuEnum = MainMenuEnum.fromId(selectedOption);
    		}
    	}while( mainMenuEnum == null );
    	
    	Task task = mainMenuEnum.task();
    	task.perform();
    }
    
    public static void endMenu(){
    	StringBuilder sb = new StringBuilder();
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	sb.append("|                                      MENU                                       |").append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");    	
    	sb.append("|    Press:                                                                       |").append("\n");
		sb.append("|        ").append(String.format("%2s", "1")).append(" ==> ").append(String.format("%-22s", "To principal menu")).append("                                            |").append("\n");
		sb.append("|        ").append(String.format("%2s", "2")).append(" ==> ").append(String.format("%-22s", "To finish")).append("                                            |").append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");    	
    	String optionSelect = getText(sb.toString(), Arrays.asList("1", "2"));
    	if( optionSelect.equals("1") ){
    		showMainMenu();
    	}
    }
    
    public static void print(String requestText){
    	System.out.println(requestText);
    }
    
    public static String getString(){
    	try{
    		return BR.readLine();
    	}catch(IOException ex){}
    	
    	return null;
    }
    
    public static String getText(String requestText){
    	return getText(requestText, null);
    }
    
    public static String getText(String requestText, List<String> allowedValues){
    	String stringReturn = null;
    	do{
    		print(requestText);
    		String temp = getString();
    		if( temp != null && temp.trim() != "" ){
    			if( allowedValues == null || allowedValues.contains(temp) ){
    				stringReturn = temp;
    			}
    		}else{
    			print("Invalid text... please try again.");
    		}
    		
    	}while(stringReturn == null);
    	
    	return stringReturn;
    }
    
    public static Integer getInt(String requestText){
    	Integer returnInteger = null;
    	do{
    		print(requestText);
    		String strNumber = getString();
    		if( strNumber != null ){
    			try{
    				returnInteger = Integer.valueOf(strNumber);
    			}catch(NumberFormatException ex){
    				print("Invalid number... please try again.");
    			}
    		}
    	}while(returnInteger == null);
    	return returnInteger;
    }
    
    public static Date getDate(String requestText, String dateFormat){
    	Date returnDate = null;
    	do{
        	print(requestText);
        	String strDate = getString();
        	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        	try{
        		returnDate = simpleDateFormat.parse(strDate);
        	}catch(ParseException ex){
        		print("Invalid date... please try again.");
        	}
    	}while(returnDate == null);
    	
    	return returnDate;
    }
    
    public static Double getDouble(String requestText){
    	Double returnDouble = null;
    	do{
    		print(requestText);
    		String strDouble = getString();
    		try{
    			returnDouble = Double.parseDouble(strDouble);
    		}catch(NumberFormatException ex){
    			print("Invalid number... please try again.");
    		}
    	}while(returnDouble == null);
    	return returnDouble;
    }
}
