package com.jpmorgan.enums;

import java.util.Calendar;

public enum CurrencyEnum {
	
	SGP(Calendar.SUNDAY, Calendar.THURSDAY), AED(Calendar.SUNDAY, Calendar.THURSDAY), OTHER(Calendar.MONDAY, Calendar.FRIDAY);
	
	private int workWeekStartDay;
	private int workWeekEndDay;
	private String currencyCode;
	
	private CurrencyEnum(int workWeekStartDay, int workWeekEndDay){
		this.workWeekStartDay = workWeekStartDay;
		this.workWeekEndDay = workWeekEndDay;
	}

	public int getWorkWeekStartDay() {
		return workWeekStartDay;
	}

	public int getWorkWeekEndDay() {
		return workWeekEndDay;
	}
	
	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public static CurrencyEnum byId(String id){
		CurrencyEnum currencyEnum = null;
		for( CurrencyEnum currency : CurrencyEnum.values() ){
			if( currency.toString().equals(id.toUpperCase()) ){
				currencyEnum = currency;
				break;
			}
		}
		if( currencyEnum == null ){
			currencyEnum = CurrencyEnum.OTHER;
		}
		
		currencyEnum.setCurrencyCode(id.toUpperCase());
		return currencyEnum;
	}
	
}
