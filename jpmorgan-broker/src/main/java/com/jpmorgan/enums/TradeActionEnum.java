package com.jpmorgan.enums;

public enum TradeActionEnum {
	
	BUY("B"){
		@Override
		public String toString() {
			return "BUY";
		}
		
	}, SELL("S"){
		@Override
		public String toString() {
			return "SELL";
		}
	};
	
	private String id;
	
	private TradeActionEnum(String id){
		this.id = id;
	}

	@Override
	public String toString() {
		return this.id;
	}
	
	public String getId(){
		return this.id;
	}
	
	public static TradeActionEnum fromId(final String id){
		for( TradeActionEnum instruction : TradeActionEnum.values() ){
			if( instruction.getId().equals(id.toUpperCase()) ){
				return instruction;
			}
		}
		return null;
	}

}
