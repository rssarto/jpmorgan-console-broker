package com.jpmorgan.enums;

import com.jpmorgan.console.tasks.InstructionTask;
import com.jpmorgan.console.tasks.ReportTask;
import com.jpmorgan.console.tasks.Task;
import com.jpmorgan.serviceImpl.InstructionServiceImpl;

public enum MainMenuEnum {
	
	CREATE_INSTRUCTION("To Send an instruction", 1){
		@Override
		public Task task() {
			return new InstructionTask(new InstructionServiceImpl());
		}
		
	}, INSTRUCTION_REPORT("To Instruction report", 2){

		@Override
		public Task task() {
			return new ReportTask(new InstructionServiceImpl());
		}
		
	};
	
	private String description;
	private int id;
	
	private MainMenuEnum(String description, int id){
		this.description = description;
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public int getId() {
		return id;
	}
	
	public static MainMenuEnum fromId(int id){
		for( MainMenuEnum menuEnum : MainMenuEnum.values() ){
			if( menuEnum.getId() == id ){
				return menuEnum;
			}
		}
		return null;
	}
	
	public abstract Task task();

}
