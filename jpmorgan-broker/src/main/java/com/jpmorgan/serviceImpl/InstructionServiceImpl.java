package com.jpmorgan.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jpmorgan.common.Instruction;
import com.jpmorgan.enums.TradeActionEnum;
import com.jpmorgan.service.InstructionService;

public class InstructionServiceImpl implements InstructionService {
	
	private static final Map<Long, List<Instruction>> INCOMING = new HashMap<>();
	private static final Map<Long, List<Instruction>> OUTGOING = new HashMap<>();

	@Override
	public Instruction receiveInstruction(Instruction instruction) {
		if( instruction == null ){
			throw new IllegalArgumentException("Instruction argument cannot be null");
		}
		instruction.setInstructionDate(getFromCalendar(new Date()));
		
		Calendar settlementCalendar = Calendar.getInstance();
		settlementCalendar.setTime(getFromCalendar(instruction.getInstructionDate()));
		while( !(settlementCalendar.get(Calendar.DAY_OF_WEEK) >= instruction.getCurrency().getWorkWeekStartDay() && settlementCalendar.get(Calendar.DAY_OF_WEEK) <= instruction.getCurrency().getWorkWeekEndDay()) ){
			settlementCalendar.add(Calendar.DATE, +1);
		}
		instruction.setSettlementDate(getFromCalendar(settlementCalendar.getTime()));
		
		List<Instruction> instructions = null;
		switch(instruction.getActionType()){
			case SELL:{
				instructions = InstructionServiceImpl.INCOMING.get(instruction.getInstructionDate().getTime());
				if( instructions == null ){
					instructions = new ArrayList<>();
					InstructionServiceImpl.INCOMING.put(instruction.getInstructionDate().getTime(), instructions);
				}
				break;
			}
			case BUY:{
				instructions = InstructionServiceImpl.OUTGOING.get(instruction.getInstructionDate().getTime());
				if( instructions == null ){
					instructions = new ArrayList<>();
					InstructionServiceImpl.OUTGOING.put(instruction.getInstructionDate().getTime(), instructions);
				}
			}
		}
		instructions.add(instruction);
		
		return instruction;
	}

	@Override
	public double getAmount(TradeActionEnum action, Date date) {
		if( action == null ){
			throw new IllegalArgumentException("action argument cannot be null.");
		}
		
		if( date == null ){
			throw new IllegalArgumentException("date argument cannot be null.");
		}
		
		List<Instruction> instructions = getInMemoryInstructionCollection(action, date);
		
		if( instructions != null && !instructions.isEmpty() ){
			double amount = 0;
			for( Instruction instruction : instructions ){
				amount += instruction.getUsdAmount();
			}
			return amount;
		}
		
		return 0;
	}

	private List<Instruction> getInMemoryInstructionCollection(TradeActionEnum action, Date date) {
		List<Instruction> instructions = null;
		switch( action ){
			case BUY:{
				instructions = InstructionServiceImpl.OUTGOING.get(this.getFromCalendar(date).getTime());
				break;
			}
			case SELL:{
				instructions = InstructionServiceImpl.INCOMING.get(this.getFromCalendar(date).getTime());
			}
		}
		if( instructions != null )
			Collections.sort(instructions);
		return instructions;
	}

	@Override
	public List<Instruction> getRankedInstructions(TradeActionEnum action, Date date, int serie) {
		List<Instruction> returnInstructions = null;
		
		if( action == null ){
			throw new IllegalArgumentException("action argument cannot be null.");
		}
		
		if( date == null ){
			throw new IllegalArgumentException("date argument cannot be null.");
		}
		
		List<Instruction> instructions = getInMemoryInstructionCollection(action, date);
		if( instructions != null && !instructions.isEmpty() ){
			if( serie > 0 && (instructions.size() > serie ) ){
				instructions.subList(0, serie - 1);
			}else{
				returnInstructions = new ArrayList<>(instructions);
			}
		}
		
		return returnInstructions;
	}
	
	public Date getFromCalendar(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

}
