package com.jpmorgan.service;

import java.util.Date;
import java.util.List;

import com.jpmorgan.common.Instruction;
import com.jpmorgan.enums.TradeActionEnum;

public interface InstructionService {
	
	Instruction receiveInstruction(Instruction instruction);
	double getAmount(TradeActionEnum action, Date date);
	List<Instruction> getRankedInstructions(TradeActionEnum action, Date date, int serie);

}
