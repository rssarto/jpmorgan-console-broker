package com.jpmorgan.console.tasks;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.jpmorgan.app.App;
import com.jpmorgan.common.Instruction;
import com.jpmorgan.enums.TradeActionEnum;
import com.jpmorgan.service.InstructionService;

public class ReportTask implements Task {
	
	private InstructionService instructionService;
	
	public ReportTask(InstructionService instructionService){
		this.instructionService = instructionService;
	}

	@Override
	public void perform() {
		Date reportDate = App.getDate("Please type the date - " + App.DEFAULT_DATE_FORMAT,App.DEFAULT_DATE_FORMAT);;
		
		double incomingAmount = instructionService.getAmount(TradeActionEnum.SELL, reportDate);
		double outgoingAmount = instructionService.getAmount(TradeActionEnum.BUY, reportDate);
		List<Instruction> incomingInstructions = instructionService.getRankedInstructions(TradeActionEnum.SELL, reportDate, 10);
		List<Instruction> outgoingInstructions = instructionService.getRankedInstructions(TradeActionEnum.BUY, reportDate, 10);
		
		SimpleDateFormat sdFormat = new SimpleDateFormat("MM/dd/yyyy");
		DecimalFormat df = new DecimalFormat("###,###,###,##0.00");
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	sb.append("|                             AMOUNT IN USD on ").append(sdFormat.format(reportDate)).append("                         |").append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
		sb.append("|    Incoming amount: ").append(String.format("USD %18s", df.format(incomingAmount))).append("                                      |").append("\n");
		sb.append("|    Outgoing amount: ").append(String.format("USD %18s", df.format(outgoingAmount))).append("                                      |").append("\n");
		sb.append("+---------------------------------------------------------------------------------+").append("\n");
		sb.append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	sb.append("|                             RANKED ENTITIES on ").append(sdFormat.format(reportDate)).append("                       |").append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
    	sb.append("|                                                                                 |").append("\n");
    	if( incomingInstructions != null && !incomingInstructions.isEmpty() ){
    		sb.append("|    Incoming:                                                                    |").append("\n");
    		for( Instruction instruction : incomingInstructions ){
    			sb.append("|").append("        ").append(String.format("%20s: ", instruction.getEntity())).append(String.format("USD %18s", df.format(instruction.getUsdAmount()))).append("                             |").append("\n");
    		}
    	}else{
    		sb.append("|    Incoming:   No recorded entities                                             |").append("\n");
    	}
    	sb.append("|                                                                                 |").append("\n");
    	if( outgoingInstructions != null && !outgoingInstructions.isEmpty() ){
    		sb.append("|    Outgoing:                                                                    |").append("\n");
    		for( Instruction instruction : outgoingInstructions ){
    			sb.append("|").append("        ").append(String.format("%20s: ", instruction.getEntity())).append(String.format("USD %18s", df.format(instruction.getUsdAmount()))).append("                             |").append("\n");	
    		}
    	}else{
    		sb.append("|    Outgoing:   No recorded entities                                             |").append("\n");
    	}
    	sb.append("|                                                                                 |").append("\n");
    	sb.append("+---------------------------------------------------------------------------------+").append("\n");
		
		
		App.print(sb.toString());
		App.endMenu();
	}

}
