package com.jpmorgan.console.tasks;

public interface Task {
	
	void perform();

}
