package com.jpmorgan.console.tasks;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.jpmorgan.app.App;
import com.jpmorgan.common.Instruction;
import com.jpmorgan.enums.CurrencyEnum;
import com.jpmorgan.enums.TradeActionEnum;
import com.jpmorgan.service.InstructionService;

public class InstructionTask implements Task {
	
	private InstructionService instructionService;
	
	public InstructionTask(InstructionService instructionService){
		this.instructionService = instructionService;
	}

	@Override
	public void perform() {
		String entity = App.getText("Please type the entity name.");
		
		StringBuilder sb = new StringBuilder();
		sb.append("Please type the Instruction type: ");
		List<String> allowedValues = new ArrayList<>();
		for( TradeActionEnum tradeActionEnum : TradeActionEnum.values() ){
			sb.append("\n\t").append(tradeActionEnum.getId()).append(" for ").append(tradeActionEnum.toString());
			allowedValues.add(tradeActionEnum.getId());
		}
		String strTradeAction = App.getText(sb.toString(), Arrays.asList(TradeActionEnum.BUY.getId(), TradeActionEnum.SELL.getId()));
		Double agreedFx = App.getDouble("Please type the Agreed Fx.");
		String currency = App.getText("Please type the currency code (eg.:  SGP, AED, USD, BRL).");
		Integer units = App.getInt("Please type the units.");
		Double pricePerUnit = App.getDouble("Please type the price per unit.");
		
		Instruction instruction = new Instruction(entity, TradeActionEnum.fromId(strTradeAction), agreedFx, CurrencyEnum.byId(currency), units, pricePerUnit);
		instruction = instructionService.receiveInstruction(instruction);
		App.print("Instruction sent successfully.");
		
		SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");
		sb = new StringBuilder();
		sb.append("\n\t").append("Entity: ").append(instruction.getEntity());
		sb.append("\n\t").append("Instruction Type: ").append(instruction.getActionType().toString());
		sb.append("\n\t").append("Agreed Fx: ").append(instruction.getAgreedFx());
		sb.append("\n\t").append("Currency: ").append(instruction.getCurrency().getCurrencyCode());
		sb.append("\n\t").append("Instruction Date: ").append(sd.format(instruction.getInstructionDate()));
		sb.append("\n\t").append("Settlement Date: ").append(sd.format(instruction.getSettlementDate()));
		sb.append("\n\t").append("Units: ").append(instruction.getUnits());
		sb.append("\n\t").append("Price Per Unit: ").append(instruction.getPricePerUnit());
		sb.append("\n\t").append("USD Amount: ").append(instruction.getUsdAmount());
		App.print(sb.toString());
		App.endMenu();
	}

}
