package com.jpmorgan.common;

import java.util.Date;

import com.jpmorgan.enums.CurrencyEnum;
import com.jpmorgan.enums.TradeActionEnum;

public class Instruction implements Comparable<Instruction> {
	
	private String entity;
	private TradeActionEnum actionType;
	private double agreedFx;
	private CurrencyEnum currency;
	private Date instructionDate;
	private Date settlementDate;
	private int units;
	private double pricePerUnit;
	private double usdAmount;
	
	public Instruction(Instruction instruction){
		this.entity = instruction.getEntity();
		this.actionType = instruction.getActionType();
		this.agreedFx = instruction.getAgreedFx();
		this.currency = instruction.getCurrency();
		this.instructionDate = instruction.getInstructionDate();
		this.units = instruction.getUnits();
		this.pricePerUnit = instruction.getPricePerUnit();
		this.usdAmount = instruction.getUsdAmount();		
	}
	
	public Instruction(String entity, TradeActionEnum actionType, double agreedFx, CurrencyEnum currency,
			int units, double pricePerUnit) {
		
		if( entity == null || entity.trim().equals("") ){
			throw new IllegalArgumentException("entity argument cannot be null or empty.");
		}
		
		if( actionType == null ){
			throw new IllegalArgumentException("actionType argument cannot be null or empty.");
		}
		
		if( !(agreedFx > 0.00) ){
			throw new IllegalArgumentException("agreedFx needs to be greater than zero.");
		}
		
		if( currency == null ){
			throw new IllegalArgumentException("currency argument cannot be null.");
		}
		
		if( !(units > 0) ){
			throw new IllegalArgumentException("units argument needs to be greater than zero.");
		}
		
		if( !(pricePerUnit > 0.00) ){
			throw new IllegalArgumentException("pricePerUnit argument needs to be greater than zero.");
		}
		
		this.entity = entity.toUpperCase();
		this.actionType = actionType;
		this.agreedFx = agreedFx;
		this.currency = currency;
		this.units = units;
		this.pricePerUnit = pricePerUnit;
		this.usdAmount = this.pricePerUnit * this.units * this.agreedFx;
	}
	
	public String getEntity() {
		return entity;
	}
	public TradeActionEnum getActionType() {
		return actionType;
	}
	public double getAgreedFx() {
		return agreedFx;
	}
	public CurrencyEnum getCurrency() {
		return currency;
	}
	public Date getInstructionDate() {
		return instructionDate;
	}
	public Date getSettlementDate() {
		return settlementDate;
	}
	
	public void setSettlementDate(Date settlementDate) {
		if( settlementDate == null ){
			throw new IllegalArgumentException("settlementDate argument cannot be null.");
		}
		
		this.settlementDate = settlementDate;
	}
	
	public void setInstructionDate(Date instructionDate) {
		if( instructionDate == null ){
			throw new IllegalArgumentException("instructionDate argument cannot be null.");
		}		
		
		this.instructionDate = instructionDate;
	}

	public int getUnits() {
		return units;
	}
	public double getPricePerUnit() {
		return pricePerUnit;
	}

	public double getUsdAmount() {
		return usdAmount;
	}

	@Override
	public int compareTo(Instruction other) {
		return new Double(other.getUsdAmount()).compareTo(new Double(this.getUsdAmount()));
	}

	@Override
	public String toString() {
		return "Instruction [entity=" + entity + ", actionType=" + actionType + ", agreedFx=" + agreedFx + ", currency="
				+ currency + ", instructionDate=" + instructionDate + ", settlementDate=" + settlementDate + ", units="
				+ units + ", pricePerUnit=" + pricePerUnit + ", usdAmount=" + usdAmount + "]";
	}
}
