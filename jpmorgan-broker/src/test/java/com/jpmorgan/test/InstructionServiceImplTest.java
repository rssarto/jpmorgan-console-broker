package com.jpmorgan.test;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.jpmorgan.common.Instruction;
import com.jpmorgan.enums.CurrencyEnum;
import com.jpmorgan.enums.TradeActionEnum;
import com.jpmorgan.service.InstructionService;
import com.jpmorgan.serviceImpl.InstructionServiceImpl;

public class InstructionServiceImplTest {
	
	private InstructionService instructionService;
	
	@Before
	public void createObjects(){
		instructionService = new InstructionServiceImpl();
	}
	
	@Test
	public void sendBuyInstruction(){
		Instruction buyInstruction = this.sendInstruction("VFR", TradeActionEnum.BUY, 1.55, CurrencyEnum.byId("BRL"), 10, 1.00);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(buyInstruction.getSettlementDate());
		Assert.assertTrue( calendar.get(Calendar.DAY_OF_WEEK) >= buyInstruction.getCurrency().getWorkWeekStartDay() && calendar.get(Calendar.DAY_OF_WEEK) <= buyInstruction.getCurrency().getWorkWeekEndDay() );
	}
	
	@Test
	public void sendSellInstruction(){
		Instruction sellInstruction = this.sendInstruction("VFR", TradeActionEnum.SELL, 1.55, CurrencyEnum.byId("BRL"), 10, 1.00);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(sellInstruction.getSettlementDate());
		Assert.assertTrue( calendar.get(Calendar.DAY_OF_WEEK) >= sellInstruction.getCurrency().getWorkWeekStartDay() && calendar.get(Calendar.DAY_OF_WEEK) <= sellInstruction.getCurrency().getWorkWeekEndDay() );
	}
	
	@Test
	public void checkIncomingAmount(){
		double currentIncomingAmount = instructionService.getAmount(TradeActionEnum.SELL, new Date());
		Instruction sellInstruction = this.sendInstruction("VFR", TradeActionEnum.SELL, 1.55, CurrencyEnum.byId("BRL"), 10, 1.00);
		currentIncomingAmount += sellInstruction.getUsdAmount();
		Assert.assertTrue(currentIncomingAmount == instructionService.getAmount(TradeActionEnum.SELL, new Date()));
	}
	
	@Test
	public void checkOutgoingAmount(){
		double currentOutgoingAmount = instructionService.getAmount(TradeActionEnum.BUY, new Date());
		Instruction buyInstruction = this.sendInstruction("VFR", TradeActionEnum.BUY, 1.55, CurrencyEnum.byId("BRL"), 10, 1.00);
		currentOutgoingAmount += buyInstruction.getUsdAmount();
		Assert.assertTrue(currentOutgoingAmount == instructionService.getAmount(TradeActionEnum.BUY, new Date()));
	}
	
	@Test
	public void testIncomingRankedEntities(){
		this.testRankedEntities(TradeActionEnum.SELL);
	}
	
	@Test
	public void testOutgoingRankedEntities(){
		this.testRankedEntities(TradeActionEnum.BUY);
	}
	
	private void testRankedEntities(TradeActionEnum actionType){
		this.sendInstruction("VFR", actionType, 1.00, CurrencyEnum.byId("BRL"), 10, 10.00);
		this.sendInstruction("PACTUAL", actionType, 2.00, CurrencyEnum.byId("BRL"), 10, 10.00);
		this.sendInstruction("THOMSON", actionType, 3.00, CurrencyEnum.byId("BRL"), 10, 10.00);
		
		List<Instruction> instructions = instructionService.getRankedInstructions(TradeActionEnum.BUY, new Date(), 10);
		Assert.assertNotNull(instructions);
		
		Double auxAmount = null;
		for( Instruction instruction : instructions ){
			if( auxAmount != null ){
				Assert.assertTrue(auxAmount > instruction.getUsdAmount());
			}
			auxAmount = instruction.getUsdAmount();
		}
	}
	
	private Instruction sendInstruction(String entity, TradeActionEnum tradeAction, double agreedFx, CurrencyEnum currencyEnum, int unitQuantity, double unitPrice){
		Instruction sellInstruction = new Instruction(entity, tradeAction, agreedFx, currencyEnum, unitQuantity, unitPrice);
		sellInstruction = instructionService.receiveInstruction(sellInstruction);
		return sellInstruction;
	}
	
}
